import unittest
from ..src.Day6 import redistribute, part1, part2


class Test(unittest.TestCase):
    def setup(self):
        pass

    def test_redistribution(self):
        working_data = [0, 2, 7, 0]
        working_data = redistribute(working_data)
        self.assertEqual(working_data, [2, 4, 1, 2], "Redistribute Broke on round 1")
        working_data = redistribute(working_data)
        self.assertEqual(working_data, [3, 1, 2, 3], "Redistribute Broke on round 2")
        working_data = redistribute(working_data)
        self.assertEqual(working_data, [0, 2, 3, 4], "Redistribute Broke on round 3")
        working_data = redistribute(working_data)
        self.assertEqual(working_data, [1, 3, 4, 1], "Redistribute Broke on round 4")
        working_data = redistribute(working_data)
        self.assertEqual(working_data, [2, 4, 1, 2], "Redistribute Broke on round 5")

    def test_part1(self):
        working_data = [0, 2, 7, 0]
        self.assertEqual(part1(working_data), 5, "Part1 Failed to find the loop")

    def test_part2(self):
        working_data = [0, 2, 7, 0]
        self.assertEqual(part2(working_data), 4, "Part1 Failed to count the loop")

if __name__ == '__main__':
    unittest.main()
