import unittest
from ..src.Day2 import part1, part1_row_checksum, part2, part2_row_checksum


class Test(unittest.TestCase):
    def setup(self):
        pass

    def test_part1(self):
        lines = ['5\t1\t9\t5', '7\t5\t3', '2\t4\t6\t8']

        self.assertEqual(part1_row_checksum(lines[0]), 8, "Part1 TestA Failed")
        self.assertEqual(part1_row_checksum(lines[1]), 4, "Part1 TestB Failed")
        self.assertEqual(part1_row_checksum(lines[2]), 6, "Part1 TestC Failed")
        self.assertEqual(part1(lines), 18, "Part1 Master Test Failed")

    def test_part2(self):
        lines = ['5\t9\t2\t8', '9\t4\t7\t3', '3\t8\t6\t5']
        self.assertEqual(part2_row_checksum(lines[0]), 4, "Part2 TestA Failed")
        self.assertEqual(part2_row_checksum(lines[1]), 3, "Part2 TestB Failed")
        self.assertEqual(part2_row_checksum(lines[2]), 2, "Part2 TestC Failed")
        self.assertEqual(part2(lines), 9, "Part2 Master Test Failed")

if __name__ == '__main__':
    unittest.main()
