import unittest
from ..src.Day1 import part1, part2


class Test(unittest.TestCase):
    def setup(self):
        pass

    def test_part1(self):
        self.assertEqual(part1('1122'), 3, "Part1 TestA Failed")
        self.assertEqual(part1('1111'), 4, "Part1 TestB Failed")
        self.assertEqual(part1('1234'), 0, "Part1 TestC Failed")
        self.assertEqual(part1('91212129'), 9, "Part1 TestD Failed")

    def test_part2(self):
        self.assertEqual(part2('1212'), 6, "Part2 TestA Failed")
        self.assertEqual(part2('1221'), 0, "Part2 TestB Failed")
        self.assertEqual(part2('123425'), 4, "Part2 TestC Failed")
        self.assertEqual(part2('123123'), 12, "Part2 TestD Failed")
        self.assertEqual(part2('12131415'), 4, "Part2 TestE Failed")


if __name__ == '__main__':
    unittest.main()
