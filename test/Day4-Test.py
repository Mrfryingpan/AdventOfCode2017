import unittest
from ..src.Day4 import part2_validate_passphrase, part1_validate_passphrase


class Test(unittest.TestCase):
    def setup(self):
        pass

    def test_part1(self):
        self.assertTrue(part1_validate_passphrase('aa bb cc dd ee'), "Part1 TestA Failed")
        self.assertTrue(part1_validate_passphrase('aa bb cc dd aaa'), "Part1 TestB Failed")
        self.assertFalse(part1_validate_passphrase('aa bb cc dd aa'), "Part1 TestC Failed")

    def test_part2(self):
        self.assertTrue(part2_validate_passphrase('abcde fghij'), "Part2 TestA Failed")
        self.assertTrue(part2_validate_passphrase('a ab abc abd abf abj'), "Part2 TestB Failed")
        self.assertTrue(part2_validate_passphrase('iiii oiii ooii oooi oooo'), "Part2 TestC Failed")
        self.assertFalse(part2_validate_passphrase('abcde xyz ecdab'), "Part2 TestD Failed")
        self.assertFalse(part2_validate_passphrase('oiii ioii iioi iiio'), "Part2 TestE Failed")


if __name__ == '__main__':
    unittest.main()
