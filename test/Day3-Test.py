import unittest
from ..src.Day3 import part1, part2


class Test(unittest.TestCase):
    def setup(self):
        pass

    def test_part1(self):
        self.assertEqual(part1(1), 0, "Part1 TestA Failed")
        self.assertEqual(part1(12), 3, "Part1 TestB Failed")
        self.assertEqual(part1(23), 2, "Part1 TestC Failed")
        self.assertEqual(part1(1024), 31, "Part1 TestD Failed")

    def part2(self):
        # No Known Tests
        pass


if __name__ == '__main__':
    Test()
