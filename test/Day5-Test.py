import unittest
from ..src.Day5 import part1, part2


class Test(unittest.TestCase):
    def setup(self):
        pass

    def test_part1(self):
        self.assertEqual(part1([0, 3, 0, 1, -3]), 5, "Part1 TestA Failed")

    def test_part2(self):
        self.assertEqual(part2([0, 3, 0, 1, -3]), 10, "Part1 TestA Failed")


if __name__ == '__main__':
    unittest.main()
