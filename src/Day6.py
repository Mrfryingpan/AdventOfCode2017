from copy import copy

def redistribute(working_data):
    inx = None
    for i in range(len(working_data)):
        if inx is None or working_data[inx] < working_data[i]:
            inx = i

    working_data = copy(working_data)

    to_distribute = working_data[inx]
    working_data[inx] = 0

    while to_distribute > 0:
        inx = (inx + 1) % len(working_data)
        working_data[inx] += 1
        to_distribute -= 1

    return working_data


def part1(input):
    history = []

    working_data = input
    count = 0
    while working_data not in history:
        history.append(working_data)

        working_data = redistribute(working_data)

        count += 1

    return count


def part2(input):
    history = []

    working_data = input
    count = 0
    while working_data not in history:
        history.append(working_data)

        working_data = redistribute(working_data)

        count += 1

    return len(history) - history.index(working_data)


if __name__ == '__main__':
    with open("../input/day6.txt", 'r') as input:
        data = [int(x) for x in input.read().split('\t')]

    # data = [0, 2, 7, 0]
    print('Part1:', part1(data))
    print('Part2:', part2(data))
