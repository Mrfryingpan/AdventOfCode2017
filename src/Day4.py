def part1_validate_passphrase(passphrase):
    words = passphrase.strip().split(" ")
    if len(set(words)) != len(words):
        return False
    return True


def part1(input):
    total = 0
    invalid_count = 0
    for line in input:
        total += 1
        if not part1_validate_passphrase(line):
            invalid_count += 1
    return total - invalid_count


def part2_validate_passphrase(passphrase):
    words = list(map((lambda x: ''.join(sorted(x))), passphrase.strip().split(" ")))
    if len(set(words)) != len(words):
        return False
    return True


def part2(input):
    total = 0
    invalid_count = 0
    for line in input:
        total += 1
        if not part2_validate_passphrase(line):
            invalid_count += 1

    return total - invalid_count


if __name__ == '__main__':
    with open("../input/day4.txt", 'r') as input:
        phrases = [x for x in input]

    print('Part 1:', part1(phrases))
    print('Part 2:', part2(phrases))
