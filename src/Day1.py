def part1(input):
    last = None
    sum = 0

    for digit in [int(x) for x in input]:
        if last and last == digit:
            sum += last
        last = digit

    if input[-1:][0] == input[0]:
        sum += int(input[0])

    return sum


def part2(input):
    length = len(input)
    half_length = int(length / 2)

    sum = 0
    for i in range(length):
        if input[i] == input[(i + half_length) % length]:
            sum += int(input[i])

    return sum


if __name__ == '__main__':
    with open("../input/day1.txt", 'r') as input:
        data = input.read()

    print('Part1:', part1(data))
    print('Part2:', part2(data))
