from copy import deepcopy


def part1(instructions):
    i = 0
    count = 0
    while 0 <= i < len(instructions):
        jump = instructions[i]
        instructions[i] += 1
        i += jump
        count += 1

    return count


def part2(instructions):
    i = 0
    count = 0
    while 0 <= i < len(instructions):
        jump = instructions[i]
        if jump >= 3:
            instructions[i] -= 1
        else:
            instructions[i] += 1
        i += jump
        count += 1

    return count


if __name__ == '__main__':
    with open("../input/day5.txt", 'r') as input:
        instructions = [int(x.strip()) for x in input]

    print('Part 1:', part1(deepcopy(instructions)))
    print('Part 2:', part2(deepcopy(instructions)))
