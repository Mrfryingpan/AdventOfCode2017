def part1_row_checksum(row):
    min = None
    max = None
    for num in [int(x) for x in row.split("\t")]:
        if not min or min > num:
            min = num
        if not max or max < num:
            max = num

    return max - min


def part1(input):
    sum = 0
    for line in input:
        sum += part1_row_checksum(line)
    return sum


def part2_row_checksum(row):
    digits = [int(x) for x in row.split("\t")]
    for i in range(len(digits)):
        for j in range(i + 1, len(digits)):
            a = digits[i]
            b = digits[j]

            if b < a and a % b == 0:
                return a / b
            elif b % a == 0:
                return b / a


def part2(input):
    sum = 0
    for line in input:
        sum += part2_row_checksum(line)

    return sum


if __name__ == '__main__':
    import os
    print(os.getcwd())
    with open("../input/day2.txt", 'r') as input:
        lines = [x for x in input]

    print('Part 1:', part1(lines))
    print('Part 2:', part2(lines))
