import math

right = (1, 0)
left = (-1, 0)
up = (0, -1)
down = (0, 1)


def part1(input):
    pos = [0, 0]
    ring_size = 0
    direction = right

    for i in range(1, input):
        if direction == right:
            pos[0] += 1
            if pos[0] > ring_size:
                direction = up
                ring_size += 1
        elif direction == left:
            pos[0] -= 1
            if pos[0] <= -ring_size:
                direction = down
        elif direction == up:
            pos[1] += 1
            if pos[1] >= ring_size:
                direction = left
        elif direction == down:
            pos[1] -= 1
            if pos[1] <= -ring_size:
                direction = right

    return abs(pos[0]) + abs(pos[1])


class Grid:
    def __init__(self, scope):
        side = int(math.ceil(math.sqrt(scope))) + 3
        self.grid = [[0 for _ in range(side)] for _ in range(side)]
        self.offset_x = int(side / 2)
        self.offset_y = int(side / 2)

    def set(self, val, x, y):
        self.grid[self.offset_y + y][self.offset_x + x] = val

    def get(self, x, y):
        return self.grid[self.offset_y + y][self.offset_x + x]

    def sum_around(self, x, y):
        sum = 0
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                sum += self.get(x + i, y + j)

        return sum

    def __iter__(self):
        return self.grid.__iter__()

    def __reversed__(self):
        return self.grid.__reversed__()


def part2(input):
    grid = Grid(input)

    pos = [0, 0]
    ring_size = 0
    direction = right

    grid.set(1, *pos)
    for i in range(1, input):
        if direction == right:
            pos[0] += 1
            if pos[0] > ring_size:
                direction = up
                ring_size += 1
        elif direction == left:
            pos[0] -= 1
            if pos[0] <= -ring_size:
                direction = down
        elif direction == up:
            pos[1] += 1
            if pos[1] >= ring_size:
                direction = left
        elif direction == down:
            pos[1] -= 1
            if pos[1] <= -ring_size:
                direction = right

        sum = grid.sum_around(*pos)
        if sum > input:
            return sum
        grid.set(grid.sum_around(*pos), *pos)


if __name__ == '__main__':
    input = 368078
    print('Part 1:', part1(input))
    print('Part 2:', part2(input))
